# React Native Class Material <a name="rn-class-sup-materi"></a>

## Table of Content
- [RN Class Supplement Material](#rn-class-sup-materi)
  - [Basic Materials](#basic-materials)
  - [Week 1](#week1)
  - [Week 2](#week2)
  - [Week 3](#week3)
  - [Week 4](#week4)

# Basic Materials <a name="basic-materials"></a>

1. [Chapter 0 - Topic 1 Sejarah Aplikasi](https://binaracademy.typeform.com/to/acGBq8)
2. [Chapter 0 - Topic 2 Konsep Produk](https://binaracademy.typeform.com/to/FQc3LB)
3. [Chapter 0 - Topic 3 SDLC](https://binaracademy.typeform.com/to/ETLVS0)
4. [Chapter 0 - Topic 4 Tech Stack](https://binaracademy.typeform.com/to/PMskdV)
5. [Chapter 0 - Topic 5 Bahasa Pemrograman](https://binaracademy.typeform.com/to/czPxr9)

# Week 1 <a name="week1"></a>

- [Week 1 Material](https://drive.google.com/drive/folders/1prl2LxWdRTcHHJMCzpScMIHkUih133we?usp=sharing)

- Link References:

Introduction:
- [The 2021 Web Developer Roadmap](https://britzdm.hashnode.dev/web-developer-roadmap-for-2021)

Installation-Configuration:
- [Install Android Studio in Linux](https://www.petanikode.com/android-studio-linux/)
- [After Install Android Studio: Create icon launcher in Ubuntu](https://buildcoding.com/3-methods-to-create-android-studio-launcher-in-ubuntu/)
- [Install VS Code Extensions for Web Development](https://codeforgeek.com/best-visual-studio-code-extensions-web-development/)

Linux Command:
- [Basic linux command](https://www.pcsuggest.com/basic-linux-commands)
- [Ubuntu shorcut](https://itsfoss.com/ubuntu-shortcuts/)

HTML & CSS:
- [Intro HTML](https://www.w3schools.com/html/default.asp)
- [Getting Started with HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started)
- [HTML-CSS Layouting](https://www.codementor.io/@codementorteam/4-different-html-css-layout-techniques-to-create-a-site-85i9t1x34)
- [How CSS works](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/How_CSS_works)
- [CSS Flexbox: A Fun Way to Learn Flexbox](https://flexboxfroggy.com/#id)
- [CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [CSS Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [CSS Framework](https://getbootstrap.com/docs/4.4/getting-started/introduction/)
- [CSS When Using Important](https://css-tricks.com/when-using-important-is-the-right-choice/)
- [Responsive with Media Queries](https://www.w3schools.com/css/css_rwd_mediaqueries.asp)

GIT:
- [Intro Git](https://www.freecodecamp.org/news/what-is-git-and-how-to-use-it-c341b049ae61/)
- [Git Cheatsheet](https://education.github.com/git-cheat-sheet-education.pdf)
- [Git Tutorial](https://rogerdudler.github.io/git-guide/index.id.html)
- [Git for Beginner in Video](https://youtu.be/8JJ101D3knE)
- [Common Git commands](https://blog.bhanuteja.dev/30-git-commands-that-i-frequently-use)
- [Markdwon Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [Mastering Markdown](https://guides.github.com/features/mastering-markdown/)

# Week 2 <a name="week2"></a>

- [Week 2 Material](https://drive.google.com/drive/folders/1wkhxoDq_gZLk6XMw2Vx0XZeIYeDjYWu2?usp=sharing)

- Link References:

JavaScript Fundamentals:
- [Intro JavaScript](https://www.dewaweb.com/blog/pengenalan-javascript/)
- [Java vs JavaScript](https://www.geeksforgeeks.org/difference-between-java-and-javascript/)
- [5 JS Style Guides](https://codeburst.io/5-javascript-style-guides-including-airbnb-github-google-88cbc6b2b7aa)
- [All About JS](https://javascript.info/)
- [JS Types Data Structures](https://codeburst.io/javascript-essentials-types-data-structures-3ac039f9877b)
- [Undefined, Null, Not Defined, Undeclared](https://icalrn.id/hiruk-pikuk-undefined/)
- [JS Filter Table](https://www.w3schools.com/howto/howto_js_filter_table.asp)

Algorithm-Flowchart-Data Structure:
- [Fundamentals of Algorithms](https://www.geeksforgeeks.org/fundamentals-of-algorithms/)
- [Algorithm, Structure, and Characteristic](https://codeva.co.id/pengertian-algoritma/)
- [Algorithm Big O Analysis](https://www.geeksforgeeks.org/analysis-algorithms-big-o-analysis/)
- [Draw Flowchart Online](https://www.draw.io/)
- [What is Data Structure](https://www.educba.com/what-is-data-structure/)
- [Data Structures Part 1](https://blog.bitsrc.io/data-structures-in-javascript-part-1-8231c9a4bc8b)
- [Data Structures Part 2](https://blog.bitsrc.io/data-structures-in-javascript-part-2-d0d09b761df0)


# Week 3 <a name="week3"></a>

- [Week 3 Material](https://drive.google.com/drive/folders/1zLi63U2OepsXWPFYvUJwWrUD7QtdX2ZU?usp=sharing)

- Link References:

JS & ES:
- [JS ES6 Cheatsheet](https://devhints.io/es6)
- [Exploring ES6](https://exploringjs.com/es6/)
- [Using Default Parameters in ES6](https://css-tricks.com/using-default-parameters-es6/)
- [When & Why You Should Use ES6 Arrow Function](https://www.freecodecamp.org/news/when-and-why-you-should-use-es6-arrow-functions-and-when-you-shouldnt-3d851d7f0b26/)
- [Use Strict in JS](https://medium.com/javascript-indonesia-community/penggunaan-use-strict-pada-javascript-7bb84d6dd83f)
- [Dot Notation vs Bracket Notation](https://medium.com/dailyjs/dot-notation-vs-bracket-notation-eedea5fa8572)
- [JS Regex](https://www.tutorialspoint.com/javascript/javascript_regexp_object.htm)
- [Regex Material](https://gitlab.com/binar-telkomsel/react-native-material/-/blob/master/modules/regular-expressions.md)
- [You dont know JS](https://github.com/getify/You-Dont-Know-JS/blob/1st-ed/README.md)
- [JS Brief and ES Features](https://medium.com/@madasamy/javascript-brief-history-and-ecmascript-es6-es7-es8-features-673973394df4)
- [JS Books](https://jsbooks.revolunet.com/)
- [JS Tutorial - JavatPoint](https://www.javatpoint.com/javascript-tutorial)
- [JS Tutorial - W3Schools](https://www.w3schools.com/js/default.asp)
- [The Modern JS Tutorial](https://javascript.info/)

Package Manager:
 - [Intro Package Manager](https://www.codepolitan.com/mengenal-package-manager-dalam-pemrograman)
 - [NPM vs Yarn](https://www.keycdn.com/blog/npm-vs-yarn)
 - [How to Publish Package to NPM](https://zellwk.com/blog/publish-to-npm/)

Programming Paradigm:
- [Intro Programming Paradigm](https://www.geeksforgeeks.org/introduction-of-programming-paradigms/)
- [Functional vs OOP vs Procedural Programming](https://medium.com/@LiliOuakninFelsen/functional-vs-object-oriented-vs-procedural-programming-a3d4585557f3)
- [Functional Programming in JS](https://medium.com/@adhywiranata/mengenal-paradigma-functional-programming-di-javascript-59d5eea7e2ac)
- [Functional Programming in ES6](https://undebugable.wordpress.com/2018/08/12/catatan-belajar-functional-programming-dengan-es6/amp/)
- [Debate: Functional Programming vs OOP](https://medium.com/@sho.miyata.1/the-object-oriented-programming-vs-functional-programming-debate-in-a-beginner-friendly-nutshell-24fb6f8625cc)
- [Infographic: Functional Programming vs OOP](https://www.educba.com/functional-programming-vs-oop/)
- [Compiler vs Interpreter](https://medium.com/@larasn_/mengenal-compiler-dan-interpreter-30610c6df554)

OOP:
- [Part 1 OOP in JS: Class-Object-Constructor-Property-Method](https://medium.com/easyread/penerapan-oop-dalam-javascript-part-1-98ed3a427e77)
- [Part 2 OOP in JS: Getter-Setter](https://medium.com/easyread/penerapan-oop-dalam-javascript-part-2-822e6c4c53c8)
- [JS and OOP Part:1](https://blog.sibudi.net/javascript-dan-object-oriented-programming-oop/)
- [JS and OOP Part:2](https://blog.sibudi.net/javascript-dan-object-oriented-programming-oop-bagian-2/)
- [JS and OOP Part:3](https://blog.sibudi.net/javascript-dan-object-oriented-programming-oop-bagian-3/)
- [Classes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)
- [Implementing Private Variables in JS](https://css-tricks.com/implementing-private-variables-in-javascript/)
- [Video: Traversy Media](https://youtu.be/vDJpGenyHaA)
- [Video: Programming with Mosh](https://www.youtube.com/watch?v=PFmuCDHHpwk&t=1384s)

JS Asynchronous:
- [Blocking vs Non Blocking](https://nodejs.org/en/docs/guides/blocking-vs-non-blocking/)
- [NodeJs: Multithreaded Server, Blocking vs Non Blocking, Event Loop](https://dev.to/jorge_rockr/everything-you-need-to-know-about-node-js-lnc)
- [Using Promise, Avoid Callback Hell](http://jamesknelson.com/grokking-es6-promises-the-four-functions-you-need-to-avoid-callback-hell/#cheatsheet)
- [Why Async?](https://frontarm.com/courses/async-javascript/promises/why-async/)
- [JS Handling Asynchronous](https://medium.com/koding-kala-weekend/bagaimana-javascript-menghandle-proses-asynchronous-callback-promise-coroutine-dan-async-await-928326575289)
- [Asynchronous JS Part: 1](https://medium.com/coderupa/panduan-komplit-asynchronous-programming-pada-javascript-part-1-fca22279c056)
- [Asynchronous JS Part: 2](https://medium.com/coderupa/panduan-komplit-asynchronous-programming-pada-javascript-part-2-callback-3a717df6cfdf)
- [Asynchronous JS Part: 3](https://medium.com/coderupa/panduan-komplit-asynchronous-programming-pada-javascript-part-3-promise-819ce5d8b3c)
- [Asynchronous JS Part: 4](https://medium.com/coderupa/panduan-komplit-asynchronous-programming-pada-javascript-part-4-async-await-fc504c344238)
- [Defer vs Async](https://medium.com/@ihsansatriawan/perbedaan-defer-dan-async-dalam-load-javascript-8446c79dc782)
- [JS Tricky](https://gist.github.com/amysimmons/3d228a9a57e30ec13ab1)

JSON:
- [Todos JSON](https://jsonplaceholder.typicode.com/todos/1)
- [User JSON](https://jsonplaceholder.typicode.com/users)
- [JSON Http Request](https://www.w3schools.com/js/js_json_http.asp)


# Week 4 <a name="week4"></a>

- [Week 4 Material](https://drive.google.com/drive/folders/1EkEyVgrC8HoYrlXwKFuxG1a35la4cFJT?usp=sharing)

- Link References:

React Native:
- [All About React](https://github.com/enaqx/awesome-react#react-general-tutorials)
- [React Native Ultimate Guide](https://www.netguru.com/blog/react-native-ultimate-guide-learning-resources-and-courses-tools-open-source-and-more)
- [7 Reasons to Choose React Native](https://geekflare.com/react-native-for-mobile-app/)
- [When React Native is not a Good Choice for a Mobile Application Development](https://www.netguru.com/blog/is-react-native-good)
- [Summary React Native at Airbnb](https://softwareengineeringdaily.com/2018/09/24/show-summary-react-native-at-airbnb/)
- [Repo: RN Community](https://github.com/react-native-community/)
- [Tips: Absolute Import Path in React Native](https://ospfolio.com/two-way-to-make-absolute-import-path-in-react-native/)
- [Tips: Direct Manipulation in React-Native](https://medium.com/@payalmaniyar/deep-understanding-of-ref-direct-manipulation-in-react-native-e89726ddb78e)
- [Video: Setup ESLint](https://www.youtube.com/watch?v=4AsOTDdmx_I&feature=youtu.be)

App Security:
- [Intro to Web App Security](http://enterprisewebbook.com/ch9_security.html)
- [Encoding, Obfuscation, Hashing, and Encryption](https://www.codepolitan.com/apa-itu-encoding-obfuscation-hashing-dan-encryption-58bfb7eee3215)
- [Charset, Encoding-Decoding](https://jagowebdev.com/character-set-dan-character-encoding/)
- [Hash and Blockchain](https://steemit.com/indonesia/@iqbalsweden/hash-dan-perannya-pada-sistem-keamanan-blockchain)
- [RN Security](https://reactnative.dev/docs/security)
- [RN Keychain/Keystore](https://github.com/oblador/react-native-keychain#installation)
- [Secure Your RN App](https://callstack.com/blog/secure-your-react-native-app/)
- [Why You Cant secure a RN App](https://blog.kuzzle.io/why-you-cant-secure-a-frontend-application)
- [JWT Handbook](https://drive.google.com/file/d/1u2THhYKmVZu1mP-V7UWgyTeXmvxWR3qv/view?usp=sharing)

Authentication-Authorization:
- [oAuth2](https://rizkimufrizal.github.io/belajar-oauth2/)
- [Auth using Firebase](https://www.freecodecamp.org/news/how-to-add-authentication-to-react-native-in-three-steps-using-firebase/)
- [Async Storage-Firebase](https://www.instamobile.io/react-native-tutorials/asyncstorage-example-react-native/)
- [Auth with 3rd Party](https://codeburst.io/react-authentication-with-twitter-google-facebook-and-github-862d59583105)
